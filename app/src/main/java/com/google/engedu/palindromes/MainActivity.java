/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.engedu.palindromes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Range;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();
    private HashMap<Range, PalindromeGroup> findings = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public boolean onFindPalindromes(View view) {
        findings.clear();
        EditText editText = (EditText) findViewById(R.id.editText);
        TextView textView = (TextView) findViewById(R.id.textView);
        String text = editText.getText().toString();
        text = text.replaceAll(" ", "");
        text = text.replaceAll("'", "");
        char[] textAsChars = text.toCharArray();
        if (isPalindrome(textAsChars, 0, text.length())) {
          textView.setText(text + " is already a palindrome!");
        } else {
            PalindromeGroup palindromes = breakIntoPalindromes(text.toCharArray(), 0, text.length());
            textView.setText(palindromes.toString());
        }
        return true;
    }

    private boolean isPalindrome(char[] text, int start, int end) {
        for (int i = start; i < end; i++) {
            if (text[i] != text[end - i -1]) {
                return false;
            }
        }
        return true;
    }

    private PalindromeGroup breakIntoPalindromes(char[] text, int start, int end) {
        PalindromeGroup palindromeGroup1 = null;
        PalindromeGroup palindromeGroup2 = null;
        for (int i = end; i > start; i--) {
            if (isPalindrome(text, start, i)) {
                palindromeGroup1 = new PalindromeGroup(text, start, i);
                palindromeGroup2 = breakIntoPalindromes(text, i, end);
                break;
            }
        }
        if (palindromeGroup2 == null) {
            return palindromeGroup1;
        }
        if (palindromeGroup1.toString().length() >= palindromeGroup2.toString().length()) {
            return palindromeGroup1;
        } else {
            return palindromeGroup2;
        }
    }
}
